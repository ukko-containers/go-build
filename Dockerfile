FROM golang:1.15.5-alpine3.12

# Setup the default environment variables for go project builds
ENV GOOS=linux
ENV GOARCH=amd64
ENV CMD_DIR=cmd
ENV APP_NAME=appie

RUN apk update && apk add bash
# Create directories for project source code and binary output
RUN mkdir /out /src

# Set the working directory to the source root
WORKDIR /src

# Define volumes for host mapping
VOLUME ["/out", "/src"]

ADD build.sh /build.sh

# Run the build command
CMD ["/build.sh"]


